package hello;


import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

import org.junit.Test;

public class HelloExpertusTest {

	private HelloExpertus greeter = new HelloExpertus(1, "Jean Paul");

	@Test
	public void greeterSaysHello() {
		assertThat(greeter.sayHelloExpertus(), containsString("Hello Expertus"));
	}

}