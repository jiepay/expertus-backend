package repo;

import profile.*;

/** 
 * Basic Search and Utility functions for our fake repository
 */
public interface IUserProfileRepository
{
    UserProfile findUserById(String id);
    UserProfile findUserByEmail(String email);
    boolean doesUserExist(String email);
}