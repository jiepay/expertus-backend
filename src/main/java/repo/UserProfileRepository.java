package repo;

import java.util.*;

import org.springframework.cache.annotation.Cacheable;

import profile.*;
import finance.*;

/**
 * A fake repository for the purpose of the assignment
 */
public class UserProfileRepository implements IUserProfileRepository
{
    private ArrayList<UserProfile> userProfiles;

    public UserProfileRepository()
    {
        try
        {
            init();
        }
        catch (Exception e)
        {
            //TODO: log e.getMessage()
        }
    }

    @Override
    @Cacheable("userId")
    public UserProfile findUserById(String id) 
    {
        //linear search - assuming very small data set
        for(UserProfile user : userProfiles)
        {
            if (user.getId().equals(id))
            {
                return user;
            }
        }
        return null;
    }

    @Override
    @Cacheable("email")
    public UserProfile findUserByEmail(String email)
    {
        //linear search - assuming very small data set
        for(UserProfile user : userProfiles)
        {        
            if (user.getEmail().equals(email))
            {
                return user;
            }
        }
        return null;
    }

    @Override
    @Cacheable("userExist")
    public boolean doesUserExist(String email)
    {
        //linear search - assuming very small data set
        for(UserProfile user : userProfiles)
        {
            if (user.getEmail().equals(email))
            {
                return true;
            }
        }
        return false;
    }


    private void init() throws Exception
    {
        userProfiles = new ArrayList<UserProfile>();
        /* NOTE: The files in resources/db are used as reference for the data in the repository.
           The JSON are not used in the project (though it was my initial intention).
           To keep things simple, I have created a static repository which will be consumed
           by the web services
           */

        UserProfile user1 = new UserProfile("5bc9ca4654cffc9476af0250", "Sherri Greer","sgreer@nullpod.com");
        ArrayList<Transaction> transactionsList1 = new ArrayList<Transaction>();
        
        transactionsList1.add(new Transaction(  "5bc9cc77ee780d4a2f2cf631",
                                                "ZOINAGE",
                                                3565.64,
                                                TransactionType.Credit,
                                                "Equatorial Guinea")
                                                );
        transactionsList1.add(new Transaction(  "5bc9cc77a7b8c762e2cba04d",
                                                "GLUID",
                                                1679.50,
                                                TransactionType.Debit,
                                                "Montserrat")
                                                );
        transactionsList1.add(new Transaction(  "5bc9cc7766e02847a2527a7c",
                                                "TASMANIA",
                                                3742.62,
                                                TransactionType.Credit,
                                                "Spain")
                                                );
        user1.setTransactions(transactionsList1);  

        UserProfile user2 = new UserProfile("5bc9ca46ba236b6a011c7464", "Hinton Watts","hwatts@nullpod.com");
        ArrayList<Transaction> transactionsList2 = new ArrayList<Transaction>();
        
        transactionsList2.add(new Transaction(  "5bc9cc77769603a1a31dbeea",
                                                "SLOFAST",
                                                2445.95,
                                                TransactionType.Credit,
                                                "Sudan")
                                                );
        transactionsList2.add(new Transaction(  "5bc9cc77899a56456f51e662",
                                                "ACRODANCE",
                                                3827.78,
                                                TransactionType.Credit,
                                                "Denmark")
                                                );
        transactionsList2.add(new Transaction(  "5bc9cc775fb8eda7ffcff4e1",
                                                "HOTCAKES",
                                                1804.34,
                                                TransactionType.Credit,
                                                "Iceland")
                                                );
        user2.setTransactions(transactionsList2);   
        userProfiles.add(user1);
        userProfiles.add(user2);      
    } 
}