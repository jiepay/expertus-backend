package start;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import hello.*;

@RestController
public class HelloExpertusController {

    private static final String template = "Hello %s!";
    private final AtomicLong counter = new AtomicLong();

    /** 
     * The basic hello expertus service.
     */
    @CrossOrigin
    @RequestMapping("/hello")
    public HelloExpertus greeting(@RequestParam(value="name", defaultValue="Expertus") String name) {
        return new HelloExpertus(counter.incrementAndGet(),
                            String.format(template, name));
    }
}