package start;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import repo.UserProfileRepository;
import profile.*;

/**
 * The initial intention was to have fully functional API with Spring Security
 * 
 */
@RestController
public class UserController {
    private UserProfileRepository repository = new UserProfileRepository();

    @CrossOrigin
    @RequestMapping("/profile/{email}")
    public UserProfile getUserProfile(@PathVariable("email") String email) {     
        return repository.findUserByEmail(email);
    }

    @CrossOrigin
    @RequestMapping("/login")
    public boolean postLogin(@RequestParam(value="email") String email) {
        return repository.doesUserExist(email);
    }

    @CrossOrigin
    @RequestMapping("/logout")
    public UserProfile getLogOut(@RequestParam(value="userid") String userid) {
        return new UserProfile();
    }
}