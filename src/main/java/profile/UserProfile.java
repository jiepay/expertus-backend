package profile;

import finance.*;
import java.util.*;

/**
 * Basic Class for a User Profile that will be used by the UI mostly
 */
public class UserProfile
{
    private String id;

    private String name;
    private String email;
    private ArrayList<Transaction> transactions;

    public UserProfile() {}

    public UserProfile(String id, String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    public String getEmail()
    {
        return this.email;
    }

    public ArrayList<Transaction> getTransactions()
    {
        return this.transactions;
    }

    public void setTransactions(ArrayList<Transaction> transactions)
    {
        this.transactions = transactions;
    } 

    @Override
    public String toString()
    {
        return this.id + " " + this.name + " " + this.email;
    }
}