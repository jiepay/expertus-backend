package hello;

/**
 * Just a basic class that illustration of the HelloExpertus class
 * The function sayHelloExpertus() is not used in the project, but rather
 * the properties are returned
 */
public class HelloExpertus{
    private final long id;
    private final String content;

    public HelloExpertus(long id, String content)
    {
        this.id = id;
        this.content = content;
    }

    public long getId()
    {
        return id;
    }

    public String getContent()
    {
        return content;
    }
    
    public String sayHelloExpertus(){
        return "Hello Expertus";
    }
}