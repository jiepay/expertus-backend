package finance;

public interface ITransaction
{
    public double calculateVat(double amount);
}