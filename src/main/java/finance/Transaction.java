package finance;

public class Transaction
{
    private String id;
    private String origin;
    private double amount;
    private TransactionType transactionType; 
    private String country;
    private ITransaction vatCalculationStrategy;

    public String getId()
    { 
        return this.id; 
    }
    public String getOrigin()
    {
        return this.origin;
    }
    public double getAmount()
    {
        return this.amount;
    }
    public TransactionType getTransactionType()
    {
        return this.transactionType;
    } 
    public String getCountry()
    {
        return this.country;
    }

    public Transaction(double amount, TransactionType transactionType)
    {
        this.origin = "";
        this.amount = amount;
        this.transactionType = transactionType;
    }

    public Transaction(String origin, double amount, TransactionType transactionType)
    {
        this.origin = origin;
        this.amount = amount;
        this.transactionType = transactionType;
    }

    public Transaction(String id, String origin, double amount,
     TransactionType transactionType, String country)
    {
        this.id = id;
        this.origin = origin;
        this.amount = amount;
        this.transactionType = transactionType;
        this.country = country;
    }

    public Transaction()
    {
        this.origin = "";
        this.amount = 0.0;
        this.transactionType = TransactionType.Debit;
    }

    /**
    * For the sake of illustrating how coding to interfaces could work.
    * In a bigger project, different business logics could be plugged in 
    * real-time to this class
    */
    public void setVatStrategy(ITransaction vatCalculationStrategy)
    {
        this.vatCalculationStrategy = vatCalculationStrategy;
    }

    public double finalSum()
    {
        double sum = 0.0;
        if (vatCalculationStrategy == null)
        {
            //default value
            vatCalculationStrategy = new MauritiusVatStrategy();
        }
        sum = this.amount + this.vatCalculationStrategy.calculateVat(this.amount);
        return sum;
    }

    public String toString()
    {
        return origin + " " + String.valueOf(this.amount) + " " + transactionType.toString();
    }
}