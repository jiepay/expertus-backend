package finance;

public enum TransactionType
{
    Credit,
    Debit
}