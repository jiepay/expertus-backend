package finance;

public class RestofTheWorldVatStrategy implements ITransaction
{
    static final double taxPercent = 0.2;
    
    public double calculateVat(double amount)
    {
        return taxPercent * amount;
    }   

}