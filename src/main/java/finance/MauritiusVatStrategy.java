package finance;

public class MauritiusVatStrategy implements ITransaction
{
    static final double upperLimit = 30000;
    static final double taxPercent = 0.15;

    public double calculateVat(double amount)
    {
        //no tax on first 30,000
        if (amount < upperLimit)
        {
            return 0.0;
        }

        amount -= upperLimit;
        return taxPercent * amount;
    }
}