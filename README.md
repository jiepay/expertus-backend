# README #

Mock Project for Interview followup at Expertus, CA

### What is this repository for? ###

Build a microservice using Java that can be run locally and that can be deployed to a 'production' server.
Minimum is to have a 'Hello Expertus' webapp/webpage.

### How do I get set up? ###

* IDE and Tools used
* I have used Visual Studio Code as IDE on Ubuntu Mate 18.04
* To get the project: git clone https://jiepay@bitbucket.org/jiepay/expertus-backend.git


* Install the following environments:

    * openjdk 10.0.2 2018-07-17
    * OpenJDK Runtime Environment (build 10.0.2+13-Ubuntu-1ubuntu0.18.04.2)
    * OpenJDK 64-Bit Server VM (build 10.0.2+13-Ubuntu-1ubuntu0.18.04.2, mixed mode)

### Tools used:

* Visual Studio Code
* Java Extension Pack for Visual Studio Code (includes debugger)
* Heroku CLI
* Spring CLI
* Maven build tools
* Docker
* JUnit for tests
* Check *pom.xml* for other dependencies

### Running and Deployment
* To run: 
    mvn compile
    mvn package (to convert it to a jar)
    mvn spring-boot:run
* To pull the image created from docker

* To run from heroku 
    https://expertus-backend.herokuapp.com/
    e.g: https://expertus-backend.herokuapp.com/hello

* To Test:
    mvn test (I have only 1 test case for the hello service, which appears in the /src/test/hello)

* To deploy to heroku:
    heroku push master

* To pull the image from docker
    docker pull jiepay/expertus-backend

### Who do I talk to? ###

jp fortuno
eternalsoul@gmail.com