FROM openjdk:8-jdk-alpine
VOLUME /tmp
EXPOSE 8080
ADD target/gs-expertus-rest-service-0.1.0.jar app.jar
ENV JAVA_OPTS=""
ENV http_proxy 127.0.0.1:8080
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar